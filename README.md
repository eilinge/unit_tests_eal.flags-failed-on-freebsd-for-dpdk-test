h4. Environment

Fill in all the following as completely as possible. Use "Unknown" or "N/A" if required. Use {{{{braces}}}} to create fixed width text like this: {{braces}}.

\\

* DPDK version: Use {{make showversion}} or for a non-released version: {{git remote -v && git show-ref --heads}}    

`5ef3592c97b9b4045015a98f804121d73583bdb2`


* Other software versions: name/version for QEMU, OVS, etc. Repeat as required.

* OS:  Use {{uname -a}}

`FreeBSD 12.0-RC3`

* Compiler: Use {{gcc --version}} or similar

`gcc (FreeBSD Ports Collection) 8.2.0`

* Hardware platform: Purley/IceLake etc.

` N/A`

* NIC hardware: Niantic/Fortville etc.

 `Niantic`

* NIC firmware: if applicable

 `N/A`

h4. Test Setup
```
export RTE_TARGET=x86_64-native-bsdapp-gcc
export RTE_SDK=`pwd`
make -j 39 install T=x86_64-native-bsdapp-gcc
kldload ./x86_64-native-bsdapp-gcc/kmod/contigmem.ko
kenv hw.nic_uio.bdfs="133:0:0,133:0:1"
kldload ./x86_64-native-bsdapp-gcc/kmod/nic_uio.ko
```
h5. Steps to reproduce

List the steps to reproduce the issue.

```
./x86_64-native-bsdapp-gcc/app/test -n 1 -c f -m 64
eal_flags_autotest
```
Show the output from the previous commands.

```
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '--no-shconf' '--no-huge' '-c' '1' '-n' '2' '-m' '18'
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: WARNING! Base virtual address hint (0x100005000 != 0x802344000) not respected!
EAL:    This may cause issues with mapping memory into secondary processes
EAL: WARNING! Base virtual address hint (0x10000b000 != 0x802372000) not respected!
EAL:    This may cause issues with mapping memory into secondary processes
EAL: PCI device 0000:05:00.0 on NUMA socket 0
EAL:   probe driver: 8086:1521 net_e1000_igb
EAL:   0000:05:00.0 not managed by UIO driver, skipping
EAL: PCI device 0000:05:00.1 on NUMA socket 0
EAL:   probe driver: 8086:1521 net_e1000_igb
EAL:   0000:05:00.1 not managed by UIO driver, skipping
EAL: PCI device 0000:85:00.0 on NUMA socket 0
EAL:   probe driver: 8086:10fb net_ixgbe
EAL: PCI device 0000:85:00.1 on NUMA socket 0
EAL:   probe driver: 8086:10fb net_ixgbe
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-n' '1' '-c' '1' '--pci-whitelist' 'error' '' ''
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: failed to parse device "error"
EAL: Unable to parse device 'error'
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-n' '1' '-c' '1' '--pci-whitelist' '0:0:0' '' ''
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: failed to parse device "0:0:0"
EAL: Unable to parse device '0:0:0'
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-n' '1' '-c' '1' '--pci-whitelist' '0:error:0.1' '' ''
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: failed to parse device "0:error:0.1"
EAL: Unable to parse device '0:error:0.1'
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-n' '1' '-c' '1' '--pci-whitelist' '0:0:0.1error' '' ''
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: failed to parse device "0:0:0.1error"
EAL: Unable to parse device '0:0:0.1error'
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-n' '1' '-c' '1' '--pci-whitelist' 'error0:0:0.1' '' ''
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: failed to parse device "error0:0:0.1"
EAL: Unable to parse device 'error0:0:0.1'
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-n' '1' '-c' '1' '--pci-whitelist' '0:0:0.1.2' '' ''
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: failed to parse device "0:0:0.1.2"
EAL: Unable to parse device '0:0:0.1.2'
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-n' '1' '-c' '1' '--pci-whitelist' '00FF:09:0B.3'
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: Multi-process socket /var/run/dpdk/rte/mp_socket_66507_2265344a5d9c
EAL: Cannot get a virtual area at requested address: 0x802371000 (got 0x802374000)
EAL: Cannot attach to memzone list
EAL: FATAL: Cannot init memzone
EAL: Cannot init memzone
Error - process did not run ok with valid whitelist
Error in test_invalid_whitelist_flag()
Test Failed
```

h5. Expected Result

Explain what is the expected result in text or as an example output:

```

Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-n' '1' '-c' '1' '-r' '17'
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: invalid rank number

Usage: ./x86_64-native-bsdapp-gcc/app/test [options]

EAL common options:
  -c COREMASK         Hexadecimal bitmask of cores to run on
  -l CORELIST         List of cores to run on
                      The argument format is <c1>[-c2][,c3[-c4],...]
                      where c1, c2, etc are core indexes between 0 and 128
  --lcores COREMAP    Map lcore set to physical cpu set
                      The argument format is
                            '<lcores[@cpus]>[<,lcores[@cpus]>...]'
                      lcores and cpus list are grouped by '(' and ')'
                      Within the group, '-' is used for range separator,
                      ',' is used for single number separator.
                      '( )' can be omitted for single element group,
                      '@' can be omitted if cpus and lcores have the same value
  -s SERVICE COREMASK Hexadecimal bitmask of cores to be used as service cores
  --master-lcore ID   Core ID that is used as master
  --mbuf-pool-ops-name Pool ops name for mbuf to use
  -n CHANNELS         Number of memory channels
  -m MB               Memory to allocate (see also --socket-mem)
  -r RANKS            Force number of memory ranks (don't detect)
  -b, --pci-blacklist Add a PCI device in black list.
                      Prevent EAL from using this PCI device. The argument
                      format is <domain:bus:devid.func>.
  -w, --pci-whitelist Add a PCI device in white list.
                      Only use the specified PCI devices. The argument format
                      is <[domain:]bus:devid.func>. This option can be present
                      several times (once per device).
                      [NOTE: PCI whitelist cannot be used with -b option]
  --vdev              Add a virtual device.
                      The argument format is <driver><id>[,key=val,...]
                      (ex: --vdev=net_pcap0,iface=eth2).
  --iova-mode   Set IOVA mode. 'pa' for IOVA_PA
                      'va' for IOVA_VA
  -d LIB.so|DIR       Add a driver or driver directory
                      (can be used multiple times)
  --vmware-tsc-map    Use VMware TSC map instead of native RDTSC
  --proc-type         Type of this process (primary|secondary|auto)
  --syslog            Set syslog facility
  --log-level=<int>   Set global log level
  --log-level=<type-match>:<int>
                      Set specific log level
  -v                  Display version information on startup
  -h, --help          This help
  --in-memory   Operate entirely in memory. This will
                      disable secondary process support

EAL options for DEBUG use only:
  --huge-unlink       Unlink hugepage files after init
  --no-huge           Use malloc instead of hugetlbfs
  --no-pci            Disable PCI
  --no-hpet           Disable HPET
  --no-shconf         No shared config (mmap'd files)

EAL: FATAL: Invalid 'command line' arguments.
EAL: Invalid 'command line' arguments.
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-n' '1' '-c' '1' '-r' '16'
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: Multi-process socket /var/run/dpdk/rte/mp_socket_62846_f04a2f7006db
EAL: PCI device 0000:05:00.0 on NUMA socket 0
EAL:   probe driver: 8086:1521 net_e1000_igb
EAL: Cannot find resource for device
EAL: PCI device 0000:05:00.1 on NUMA socket 0
EAL:   probe driver: 8086:1521 net_e1000_igb
EAL: Cannot find resource for device
EAL: PCI device 0000:85:00.0 on NUMA socket 0
EAL:   probe driver: 8086:10fb net_ixgbe
EAL: Cannot mmap device resource file /dev/uio@pci:133:0:0 to address: 0x8013f7000
EAL: Requested device 0000:85:00.0 cannot be used
EAL: PCI device 0000:85:00.1 on NUMA socket 0
EAL:   probe driver: 8086:10fb net_ixgbe
eth_ixgbe_dev_init(): No TX queues configured yet. Using default TX function.
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-c' '10' '-n' '2' '-m' '18'
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: Multi-process socket /var/run/dpdk/rte/mp_socket_62847_f04b7ce80690
EAL: PCI device 0000:05:00.0 on NUMA socket 0
EAL:   probe driver: 8086:1521 net_e1000_igb
EAL: Cannot find resource for device
EAL: PCI device 0000:05:00.1 on NUMA socket 0
EAL:   probe driver: 8086:1521 net_e1000_igb
EAL: Cannot find resource for device
EAL: PCI device 0000:85:00.0 on NUMA socket 0
EAL:   probe driver: 8086:10fb net_ixgbe
EAL: Cannot mmap device resource file /dev/uio@pci:133:0:0 to address: 0x8013f7000
EAL: Requested device 0000:85:00.0 cannot be used
EAL: PCI device 0000:85:00.1 on NUMA socket 0
EAL:   probe driver: 8086:10fb net_ixgbe
eth_ixgbe_dev_init(): No TX queues configured yet. Using default TX function.
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-c' '1' '--invalid-opt'
test: unrecognized option `--invalid-opt'
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes

Usage: ./x86_64-native-bsdapp-gcc/app/test [options]

EAL common options:
  -c COREMASK         Hexadecimal bitmask of cores to run on
  -l CORELIST         List of cores to run on
                      The argument format is <c1>[-c2][,c3[-c4],...]
                      where c1, c2, etc are core indexes between 0 and 128
  --lcores COREMAP    Map lcore set to physical cpu set
                      The argument format is
                            '<lcores[@cpus]>[<,lcores[@cpus]>...]'
                      lcores and cpus list are grouped by '(' and ')'
                      Within the group, '-' is used for range separator,
                      ',' is used for single number separator.
                      '( )' can be omitted for single element group,
                      '@' can be omitted if cpus and lcores have the same value
  -s SERVICE COREMASK Hexadecimal bitmask of cores to be used as service cores
  --master-lcore ID   Core ID that is used as master
  --mbuf-pool-ops-name Pool ops name for mbuf to use
  -n CHANNELS         Number of memory channels
  -m MB               Memory to allocate (see also --socket-mem)
  -r RANKS            Force number of memory ranks (don't detect)
  -b, --pci-blacklist Add a PCI device in black list.
                      Prevent EAL from using this PCI device. The argument
                      format is <domain:bus:devid.func>.
  -w, --pci-whitelist Add a PCI device in white list.
                      Only use the specified PCI devices. The argument format
                      is <[domain:]bus:devid.func>. This option can be present
                      several times (once per device).
                      [NOTE: PCI whitelist cannot be used with -b option]
  --vdev              Add a virtual device.
                      The argument format is <driver><id>[,key=val,...]
                      (ex: --vdev=net_pcap0,iface=eth2).
  --iova-mode   Set IOVA mode. 'pa' for IOVA_PA
                      'va' for IOVA_VA
  -d LIB.so|DIR       Add a driver or driver directory
                      (can be used multiple times)
  --vmware-tsc-map    Use VMware TSC map instead of native RDTSC
  --proc-type         Type of this process (primary|secondary|auto)
  --syslog            Set syslog facility
  --log-level=<int>   Set global log level
  --log-level=<type-match>:<int>
                      Set specific log level
  -v                  Display version information on startup
  -h, --help          This help
  --in-memory   Operate entirely in memory. This will
                      disable secondary process support

EAL options for DEBUG use only:
  --huge-unlink       Unlink hugepage files after init
  --no-huge           Use malloc instead of hugetlbfs
  --no-pci            Disable PCI
  --no-hpet           Disable HPET
  --no-shconf         No shared config (mmap'd files)

EAL: FATAL: Invalid 'command line' arguments.
EAL: Invalid 'command line' arguments.
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-c' '1' '--no-pci'
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: Multi-process socket /var/run/dpdk/rte/mp_socket_62849_f04e04ac1f5a
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '' '--proc-type=secondary' '-c' '1' '-v'
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: RTE Version: 'DPDK 19.08.0-rc0'
EAL: Multi-process socket /var/run/dpdk/rte/mp_socket_62850_f04f54c283d8
EAL: PCI device 0000:05:00.0 on NUMA socket 0
EAL:   probe driver: 8086:1521 net_e1000_igb
EAL: Cannot find resource for device
EAL: PCI device 0000:05:00.1 on NUMA socket 0
EAL:   probe driver: 8086:1521 net_e1000_igb
EAL: Cannot find resource for device
EAL: PCI device 0000:85:00.0 on NUMA socket 0
EAL:   probe driver: 8086:10fb net_ixgbe
EAL: Cannot mmap device resource file /dev/uio@pci:133:0:0 to address: 0x8013f7000
EAL: Requested device 0000:85:00.0 cannot be used
EAL: PCI device 0000:85:00.1 on NUMA socket 0
EAL:   probe driver: 8086:10fb net_ixgbe
eth_ixgbe_dev_init(): No TX queues configured yet. Using default TX function.
Running binary with argv[]:'./x86_64-native-bsdapp-gcc/app/test' '-c' '1' '-n' '2' '-m' '18' '--no-shconf' '' '--no-huge'
EAL: Sysctl reports 40 cpus
EAL: Detected 40 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: WARNING! Base virtual address hint (0x100005000 != 0x80218f000) not respected!
EAL:    This may cause issues with mapping memory into secondary processes
EAL: WARNING! Base virtual address hint (0x10000b000 != 0x8021bd000) not respected!
EAL:    This may cause issues with mapping memory into secondary processes
EAL: PCI device 0000:05:00.0 on NUMA socket 0
EAL:   probe driver: 8086:1521 net_e1000_igb
EAL:   0000:05:00.0 not managed by UIO driver, skipping
EAL: PCI device 0000:05:00.1 on NUMA socket 0
EAL:   probe driver: 8086:1521 net_e1000_igb
EAL:   0000:05:00.1 not managed by UIO driver, skipping
EAL: PCI device 0000:85:00.0 on NUMA socket 0
EAL:   probe driver: 8086:10fb net_ixgbe
EAL: PCI device 0000:85:00.1 on NUMA socket 0
EAL:   probe driver: 8086:10fb net_ixgbe
Test OK
```

h4. Regression

Is this issue a regression: (Y/N)
```
N
```
Version the regression was introduced: Specify git id if known.
